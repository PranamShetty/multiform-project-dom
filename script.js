let currentPage = 0;
const pageContainer = document.querySelector(".page-container");
const togglePart = document.getElementById("toggle");
let selectedToggle = "month";
let selectedPlan = "";

pageContainer.addEventListener("click", handleButtonClick);
togglePart.addEventListener("change", () => {
  selectedToggle = togglePart.checked ? "year" : "month";
  updateSelectedToggle();
  updateToggleText();
});

function handleButtonClick(event) {
  const target = event.target;
  if (target.classList.contains("bottom-button-right")) {
    if (currentPage === 0) {
      validateFields();
    } else {
      nextPage();
    }
    updateSelectedToggle();
    updateToggleText();
    if (currentPage === 3) {
      changeText();
    }
  } else if (target.classList.contains("bottom-button-left")) {
    previousPage();
  }
}

function nextPage() {
  const pages = document.getElementsByClassName("info-details-container");
  const texts = document.getElementsByClassName("step-no");
  if (currentPage < pages.length - 1) {
    pages[currentPage].classList.add("hidden");
    texts[currentPage].classList.remove("add-White-BG");
    currentPage++;
    pages[currentPage].classList.remove("hidden");
    texts[currentPage].classList.add("add-White-BG");
  }
}

function previousPage() {
  const pages = document.getElementsByClassName("info-details-container");
  const texts = document.getElementsByClassName("step-no");
  if (currentPage > 0) {
    pages[currentPage].classList.add("hidden");
    texts[currentPage].classList.remove("add-White-BG");
    currentPage--;
    pages[currentPage].classList.remove("hidden");
    texts[currentPage].classList.add("add-White-BG");
  }
}

function updateSelectedToggle() {
  const arcadeTextElement = document.querySelector(".arcade-text");
  const yearlyPriceElement = document.querySelector(".Yearly-price");
  const onlineServicePrice = document.querySelector(".Online-service-plan-price");
  const largerStoragePrice = document.querySelector(".Larger-storage-plan-price");
  const customStoragePrice = document.querySelector(".Custom-storage-plan-price");

  onlineServicePrice.setAttribute("value", selectedToggle === "month" ? "1" : "10");

  if (selectedToggle === "month") {
    const prices = { "Arcade": 9, "Advanced": 12, "Pro": 15 };
    arcadeTextElement.textContent = `${selectedPlan}(Monthly)`;
    yearlyPriceElement.textContent = `$${prices[selectedPlan]}/mo`;
  } else {
    const prices = { "Arcade": 90, "Advanced": 120, "Pro": 150 };
    arcadeTextElement.textContent = `${selectedPlan}(Yearly)`;
    yearlyPriceElement.textContent = `$${prices[selectedPlan]}/yr`;
  }

  onlineServicePrice.innerHTML = selectedToggle === "month" ? "+$1/mo" : "+$10/yr";
  largerStoragePrice.innerHTML = selectedToggle === "month" ? "+$2/mo" : "+$20/yr";
  customStoragePrice.innerHTML = selectedToggle === "month" ? "+$2/mo" : "+$20/yr";

  updatePricing();
}

function updateToggleText() {
  const onlineCost = selectedToggle === "month" ? "$1/mo" : "$10/yr";
  const largerCost = selectedToggle === "month" ? "$2/mo" : "$20/yr";
  document.querySelector(".additional-cost-online").textContent = `+${onlineCost}`;
  document.querySelector(".additional-cost-larger").textContent = `+${largerCost}`;
  document.querySelector(".additional-cost-custom").textContent = `+${largerCost}`;
  updatePricing();
}

function updatePricing() {
  const onlineCheckbox = document.querySelector("#addon-checkbox-online");
  const largerCheckbox = document.querySelector("#addon-checkbox-larger");
  const customCheckbox = document.querySelector("#addon-checkbox-custom");
  const onlineServicePlan = document.querySelector(".Online-service-plan");
  const largerStoragePlan = document.querySelector(".Larger-storage-plan");
  const customStoragePlan = document.querySelector(".Custom-storage-plan");
  const onlineServicePrice = document.querySelector(".Online-service-plan-price");
  const largerStoragePrice = document.querySelector(".Larger-storage-plan-price");
  const customStoragePrice = document.querySelector(".Custom-storage-plan-price");

  if (onlineCheckbox.checked) {
    onlineServicePlan.classList.remove("hidden");
    onlineServicePrice.classList.remove("hidden");
    onlineServicePrice.innerHTML = selectedToggle === "month" ? "+$1/mo" : "+$10/yr";
  } else {
    onlineServicePlan.classList.add("hidden");
    onlineServicePrice.classList.add("hidden");
  }

  if (largerCheckbox.checked) {
    largerStoragePlan.classList.remove("hidden");
    largerStoragePrice.classList.remove("hidden");
    largerStoragePrice.innerHTML = selectedToggle === "month" ? "+$2/mo" : "+$20/yr";
  } else {
    largerStoragePlan.classList.add("hidden");
    largerStoragePrice.classList.add("hidden");
  }

  if (customCheckbox.checked) {
    customStoragePlan.classList.remove("hidden");
    customStoragePrice.classList.remove("hidden");
    customStoragePrice.innerHTML = selectedToggle === "month" ? "+$2/mo" : "+$20/yr";
  } else {
    customStoragePlan.classList.add("hidden");
    customStoragePrice.classList.add("hidden");
  }
}

const radioInputs = document.querySelectorAll('input[type="radio"]');
for (const radioInput of radioInputs) {
  radioInput.addEventListener("change", function () {
    if (this.checked) {
      selectedPlan = this.value;
    }
  });
}

function validateFields() {
  const nameInput = document.querySelector('.to-take-input-name');
  const emailInput = document.querySelector('.to-take-input-email');
  const phoneInput = document.querySelector('.to-take-input-phone');
  const nameError = document.querySelector('.name-error');
  const emailError = document.querySelector('.email-error');
  const phoneError = document.querySelector('.phone-error');

  function showErrorMessage(input, error) {
    if (input.value.trim() === '') {
      error.style.display = 'block';
      input.classList.add('red-border');
    } else {
      error.style.display = 'none';
      input.classList.remove('red-border');
    }
  }

  showErrorMessage(nameInput, nameError);
  showErrorMessage(emailInput, emailError);
  showErrorMessage(phoneInput, phoneError);
  if (nameInput.value.trim() !== '' && emailInput.value.trim() !== '' && phoneInput.value.trim() !== '') {
    nextPage();
  }
}

function changeText() {
  const v1 = parseInt(document.querySelector('.Yearly-price').getAttribute("value"));
  let v2 = 0;
  const prices = document.querySelector(".finishing-up-2").childNodes[3].childNodes;
  Array.from(prices).forEach(price => {
    if (!price.classList.contains("hidden")) {
      v2 += parseInt(price.getAttribute("value"))
    }
  });

  const element = document.querySelector("#page-1-container > div.page-4-container-details > div.finishing-up-3 > div:nth-child(2) > p");
  element.innerText = selectedToggle === "month" ? `$${v2 + v1}/mo` : `$${v2 + v1}/yr`;
}

// Update background color of plan containers
const planContainers = document.querySelectorAll(".arcade-container, .advanced-container, .pro-container");
for (const container of planContainers) {
  container.addEventListener("click", () => {
    for (const c of planContainers) {
      c.style.borderColor = c === container ? "hsl(243, 100%, 62%)" : "hsl(231, 11%, 63%)";
    }
  });
}

// Handle final confirm button
function handleCustomButtonClick() {
  const pages = document.getElementsByClassName("info-details-container");
  pages[3].classList.add("hidden");
  const thankyouDiv = document.querySelector(".info-details-container-thankyou");
  thankyouDiv.classList.remove("hidden");
}
document.querySelector(".bottom-button-right-2").addEventListener("click", handleCustomButtonClick);

updateSelectedToggle();
